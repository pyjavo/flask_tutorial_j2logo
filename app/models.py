import datetime

from flask import url_for
from slugify import slugify
from sqlalchemy.exc import IntegrityError

from app import db


class Post(db.Model):
    '''Clase para crear artículos del blog'''

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer,
        db.ForeignKey('blog_user.id', ondelete='CASCADE'),
        nullable=False
    )
    title = db.Column(db.String(256), nullable=True)
    title_slug = db.Column(db.String(256), unique=True, nullable=False)
    content = db.Column(db.Text)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    image_name = db.Column(db.String, nullable=True)
    comments = db.relationship(
        'Comment',
        backref='post',
        lazy=True,
        cascade='all,delete-orphan',
        order_by='asc(Comment.created)'
    )

    def __repr__(self):
        return f'<Post {self.title}>'

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def save(self):
        if not self.id:
            # Si no existe un id en la BD
            db.session.add(self)
        if not self.title_slug:
            # Si no existe un title_slug parecido en la BD
            self.title_slug = slugify(self.title)

        saved = False
        count = 0
        while not saved:
            try:
                db.session.commit()
                saved = True
            except IntegrityError:
                db.session.rollback()
                db.session.add(self)
                count += 1
                self.title_slug = f'{slugify(self.title)}-{count}'

    @staticmethod
    def get_by_slug(slug):
        '''Consulta de Post por slug'''
        return Post.query.filter_by(title_slug=slug).first()

    @staticmethod
    def get_all():
        '''Consulta de todos los posts'''
        return Post.query.all()

    @staticmethod
    def get_by_id(id):
        '''Hace una consulta por id a la base de datos'''

        return Post.query.get(id)

    @staticmethod
    def all_paginated(page=1, per_page=20):
        '''Devuelve un listado paginado de posts ordenados por fecha de creación.

        Args:
          page: indica la página a partir de la cuál obtener los resultados.
          per_page: indica cuántos elementos se devuelven en la consulta.

        Returns:
          paginate() devuelve un objeto de tipo Pagination que, además de los
          elementos devueltos, tiene campos para controlar la paginación de la consulta.
        '''
        return Post.query.order_by(Post.created.asc()).\
            paginate(page=page, per_page=per_page, error_out=False)


class Comment(db.Model):
    '''Permitir a los usuarios agregar comentarios en los posts.'''

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer,
        db.ForeignKey('blog_user.id', ondelete='SET NULL')
    )
    user_name = db.Column(db.String)
    post_id = db.Column(
        db.Integer, db.ForeignKey('post.id'), nullable=False
    )
    content = db.Column(db.Text)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def __init__(self, content, user_id=None, user_name=user_name, post_id=None):
        '''¿¿ Por qué no llama al super() ??

          ejemplo:
            https://flask-sqlalchemy.palletsprojects.com/en/2.x/quickstart/#a-minimal-application

            class Foo(db.Model):
            # ...
            def __init__(self, **kwargs):
                super().__init__(**kwargs)
                # do custom stuff
        '''
        self.content = content
        self.user_id = user_id
        self.user_name = user_name
        self.post_id = post_id

    def __repr__(self):
        return f'<Comment {self.content}>'

    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_by_post_id(post_id):
        return Comment.query.filter_by(post_id=post_id).all()
