from flask import Blueprint


# Similar a public/__init__.py
admin_bp = Blueprint(
    'admin',
    __name__,
    template_folder='templates',
)

from . import routes
