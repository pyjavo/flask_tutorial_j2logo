from functools import wraps
from flask import abort
from flask_login import current_user


def admin_required(f):
    '''Comprueba si current_user tiene el atributo is_admin en True.'''

    @wraps(f)
    def decorated_function(*args, **kws):
        is_admin = getattr(current_user, 'is_admin', False)
        if not is_admin:
            abort(401)
        return f(*args, **kws)
    return decorated_function
