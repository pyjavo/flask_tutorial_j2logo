from flask import render_template, request, redirect, url_for
from flask_login import current_user, login_user, logout_user
from werkzeug.urls import url_parse

from app import login_manager
from . import auth_bp
from .forms import SignupForm, LoginForm
from .models import User


@auth_bp.route("/signup/", methods=["GET", "POST"])
def show_signup_form():
    '''Vista para registrar a un nuevo usuario.'''

    if current_user.is_authenticated:
        return redirect(url_for('public.index'))

    form = SignupForm()
    error = None

    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        password = form.password.data
        # Comprobamos que no hay ya un usuario con ese email
        user = User.get_by_email(email)
        if user is not None:
            error = f'El email {email} ya está siendo utilizado por otro usuario'
        else:
            # Creamos el usuario y lo guardamos en BD
            user = User(name=name, email=email)
            user.set_password(password)
            user.save()

            # Dejamos al usuario logueado
            login_user(user, remember=True)

            # Redirigimos al usuario a la página que se indica en el mismo
            next_ = request.args.get('next', None)
            if not next_ or url_parse(next_).netloc != '':
                next_ = url_for('public.index')
            return redirect(next_)
    return render_template("auth/signup_form.html", form=form, error=error)


@login_manager.user_loader
def load_user(user_id):
    '''Vista para cargar usuario por id.'''

    return User.get_by_id(int(user_id))


@auth_bp.route("/login", methods=["GET", "POST"])
def login():
    '''Vista para autenticarse.'''

    # current_user es una variable de Flask_Login
    if current_user.is_authenticated:
        return redirect(url_for(index))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.get_by_email(form.email.data)
        if user is not None and user.check_password(form.password.data):
            # función login_user de Flask_login
            login_user(user, remember=form.remember_me.data)
            next_page = request.args.get('next')
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('public.index')
            return redirect(next_page)
    return render_template('auth/login_form.html', form=form)


@auth_bp.route('/logout')
def logout():
    '''Vista para cerrar sesión.'''

    logout_user()
    return redirect(url_for('public.index'))
