from flask import Blueprint


# Similar a public/__init__.py
auth_bp = Blueprint(
    'auth',
    __name__,
    template_folder='templates',
)

from . import routes
