from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db


class User(db.Model, UserMixin):
    '''Clase para crear usuarios de la app web

    Por defecto, SQLAlchemy llamaría a la tabla 'user', pero
    user es una palabra reservada de PostgreSQL.
    '''
    __tablename__ = 'blog_user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(256), unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)
    is_admin = db.Column(db.Boolean, default=False)

    # def __init__(self, name, email):
    #     self.name = name
    #     self.email = email

    def __repr__(self):
        return f'<User {self.email}>'

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(id):
        '''Hace una consulta por id a la base de datos'''

        return User.query.session.get(User, id)

    @staticmethod
    def get_by_email(email):
        '''Consulta un usuario en la base de datos dado un email.

        Reemplaza a la función get_user() de la leccion 4.

        Args:
          email:
            Correo del usuario

        Returns:
          El objeto User que coincida con el email suministrado
        '''

        return User.query.filter_by(email=email).first()

    @staticmethod
    def get_all():
        '''Consulta de todos los usuarios'''

        return User.query.all()
