import random

import click
from faker import Faker
from flask import Blueprint

from app.models import Post
from app.auth.models import User


cli = Blueprint('cli', __name__)
cli.cli.short_help = 'Commands for the CLI Blueprint'


@cli.cli.command("orale")
@click.argument('number', type=int)
def print_number(number):
    """Prueba de cli command.

    Ej:
      Corre así:
        $ flask cli orale 42
    """

    print('Parámetro recibido', type(number))
    click.echo(f"The number is: {number}")


@cli.cli.command("fake-posts")
@click.argument('number', type=int)
def create_fake_posts(number):
    """Crea artículos con Faker y se lo asigna a un usuario.

    Esto con el fin de probar el paginador de la lección 13.

    Args:
      number: número de artículos a realizar.

    Ej:
      Así se ejecuta por consola:
        $ flask cli fake-posts <number>
    """

    fake = Faker()
    users = User.get_all()
    user_ids = []

    for u in users:
        user_ids.append(u.id)

    # TODO: Verificar si se puede mejorar con una operación tipo "bulk insert"
    for inc in range(number):
      user_id = random.sample(user_ids, k=1)[0]  # Escojo un usuario al azar
      title = fake.sentence(nb_words=10, variable_nb_words=False)
      content = fake.sentence(nb_words=80, variable_nb_words=True)
      post = Post(user_id=user_id, title=title, content=content)
      post.save()

      click.echo(
        f"============ #{inc}. Se ha creado un nuevo post: ============\n{post.title}\n"
      )
