import logging
from logging.handlers import SMTPHandler

from flask import Flask, render_template
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_mail import Mail

from app.common.filters import format_datetime


login_manager = LoginManager()
db = SQLAlchemy()  # Crear el objeto SQLAlchemy
migrate = Migrate()  # Se crea un objeto de tipo Migrate
mail = Mail()  # Instanciamos un objeto de tipo Mail


def create_app(settings_module):
    '''Configurar diferentes apps a partir del mismo proyecto.

    Es una función factoría que permite inicializar y registrar
    diferentes Blueprints.
    '''

    app = Flask(__name__, instance_relative_config=True)

    # Carga el archivo config especificado por la variable de entorno APP.
    app.config.from_object(settings_module)

    # Carga la configuración de la carpeta instance
    if app.config.get('TESTING', False):
        app.config.from_pyfile('config-testing.py', silent=True)
    else:
        app.config.from_pyfile('config.py', silent=True)

    configure_logging(app)

    login_manager.init_app(app)
    # Indicar la vista para el login, Así no mostrará error 401
    login_manager.login_view = "auth.login"

    db.init_app(app)
    # Se inicializa el objeto migrate
    migrate.init_app(app, db)
    # Inicializamos el objeto mail
    mail.init_app(app)

    # Registro de los filtros
    register_filters(app)

    ######## Registro de los Blueprints ########
    from .public import public_bp
    app.register_blueprint(public_bp)

    from .admin import admin_bp
    app.register_blueprint(admin_bp)

    from .auth import auth_bp
    app.register_blueprint(auth_bp)

    from .cli import cli as cli_blueprint
    app.register_blueprint(cli_blueprint)

    ######## Manejadores de errores personalizados ########
    register_error_handlers(app)

    return app


def register_filters(app):
    '''registra la función format_datetime() como filtro de JINJA2.'''

    app.jinja_env.filters['datetime'] = format_datetime


def register_error_handlers(app):
    '''Manejadores de errores personalizados.'''

    @app.errorhandler(500)
    def base_error_handler(e):
        return render_template('500.html'), 500

    @app.errorhandler(404)
    def error_404_handler(e):
        return render_template('404.html'), 404

    @app.errorhandler(401)
    def error_404_handler(e):
        return render_template('401.html'), 401


def verbose_formatter():
    return logging.Formatter(
        '[%(asctime)s.%(msecs)d]\t %(levelname)s \t[%(name)s.%(funcName)s:%(lineno)d]\t %(message)s',
        datefmt='%d/%m/%Y %H:%M:%S'
    )


def configure_logging(app):
    '''Configurar el módulo logging.'''

    # Eliminamos los posibles manejadores, si existen, del logger por defecto
    del app.logger.handlers[:]

    # Añadimos el logger por defecto a la lista de loggers
    loggers = [app.logger, logging.getLogger('sqlalchemy'), ]
    handlers = []

    # Creamos un manejador para escribir los mensajes por consola
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(verbose_formatter())

    # Dependiendo del entorno se asigna un nivel de log y se agrega a los handlers
    if (app.config['APP_ENV'] == app.config['APP_ENV_LOCAL']) or (
            app.config['APP_ENV'] == app.config['APP_ENV_DEVELOPMENT']):
        console_handler.setLevel(logging.DEBUG)
        handlers.append(console_handler)
    elif app.config['APP_ENV'] == app.config['APP_ENV_TESTING']:
        console_handler.setLevel(logging.WARNING)
        handlers.append(console_handler)
    elif app.config['APP_ENV'] == app.config['APP_ENV_PRODUCTION']:
        console_handler.setLevel(logging.INFO)
        handlers.append(console_handler)

        mail_handler = SMTPHandler((app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
                                    app.config['DONT_REPLY_FROM_EMAIL'],
                                    app.config['ADMINS'],
                                    f"[Error][{app.config['APP_ENV']}] La aplicación falló",
                                    (app.config['MAIL_USERNAME'], app.config['MAIL_PASSWORD']),
                                    ())
        mail_handler.setLevel(logging.ERROR)
        mail_handler.setFormatter(mail_handler_formatter())
        handlers.append(mail_handler)

    # Asociamos cada uno de los handlers a cada uno de los loggers
    for l in loggers:
        for handler in handlers:
            l.addHandler(handler)
        l.propagate = False
        l.setLevel(logging.DEBUG)


def mail_handler_formatter():
    return logging.Formatter(
        '''
            Message type:       %(levelname)s
            Location:           %(pathname)s:%(lineno)d
            Module:             %(module)s
            Function:           %(funcName)s
            Time:               %(asctime)s.%(msecs)d

            Message:

            %(message)s
        ''',
        datefmt='%d/%m/%Y %H:%M:%S'
    )
