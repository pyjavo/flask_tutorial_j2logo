def format_datetime(value, format='short'):
    '''Transformar formato de fecha en JINJA2.

    Args:
      value: es la fecha a transformar
      format: el formato de salida que aplicaremos a la fecha.
    '''

    value_str = None
    if not value:
        value_str = ''

    if format == 'short':
        value_str = value.strftime('%d/%m/%Y')
    elif format == 'full':
        value_str = value.strftime('%B %d de %Y')
    else:
        value_str = ''

    return value_str
