from . import BaseTestCase
from app.auth.models import User
from app.models import Post


class BlogTestCase(BaseTestCase):
    """docstring for BlogtestCase"""

    def test_index_with_no_posts(self):
        """Prueba que aparece en el home cuando no hay entradas."""

        res = self.client.get('/')
        self.assertEqual(200, res.status_code)
        self.assertIn(b'No hay entradas', res.data)

    def test_index_with_posts(self):
        """Probar el home cuando existe al menos una entrada."""

        with self.app.app_context():
            admin = User.get_by_email('admin@xyz.com')
            post = Post(user_id=admin.id, title='Post de Prueba', content='Lorem Ipsum')
            post.save()

        res = self.client.get('/')
        self.assertEqual(200, res.status_code)
        self.assertNotIn(b'No hay entradas', res.data)

    def test_redirect_to_login(self):
        """Probar si el decorador @login_required redirige al login."""

        res = self.client.get('/admin/')
        self.assertEqual(302, res.status_code)
        self.assertIn('login', res.location)

    def test_unauthorized_access_admin(self):
        """Probar si el decorador @admin_required retorna el código 401."""

        self.login('guest@xyz.com', '1111')
        res = self.client.get('/admin/')
        self.assertEqual(401, res.status_code)
        self.assertIn(b'Ooops!! No tienes permisos de acceso', res.data)


    def test_authorized_access_admin(self):
        """Probar si un usuario administrador puede acceder a la página /admin."""

        self.login('admin@xyz.com', '1111')
        res = self.client.get('/admin/')
        self.assertEqual(200, res.status_code)
        self.assertIn(b'Posts', res.data)
        self.assertIn(b'Usuarios', res.data)
