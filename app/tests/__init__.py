import unittest

from app import create_app, db
from app.auth.models import User


class BaseTestCase(unittest.TestCase):
    """Clase base para los tests"""

    def setUp(self):
        """Se ejecuta justo antes de cada test."""

        self.app = create_app(settings_module="config.testing")
        self.client = self.app.test_client()

        # Crea un contexto de aplicación
        with self.app.app_context():
            # Crea las tablas de la base de datos
            db.create_all()
            # Creamos un usuario administrador
            BaseTestCase.create_user('admin', 'admin@xyz.com', '1111', True)
            # Creamos un usuario invitado
            BaseTestCase.create_user('guest', 'guest@xyz.com', '1111', False)

    def tearDown(self):
        """Borra las tablas de la BD tras finalizar cada test."""

        with self.app.app_context():
            # Elimina todas las tablas de la base de datos
            db.session.remove()
            db.drop_all()

    def login(self, email, password):
        """Autenticar el usuario de prueba en la aplicación."""

        return self.client.post('/login', data=dict(
            email=email,
            password=password
        ), follow_redirects=True)

    @staticmethod
    def create_user(name, email, password, is_admin):
        user = User(name=name, email=email)
        user.set_password(password)
        user.is_admin = is_admin
        user.save()
        return user
