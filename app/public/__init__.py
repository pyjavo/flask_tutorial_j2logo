from flask import Blueprint


# Nombre 'public' para el Blueprint
# Nombre __name__ para la importación (en este caso el 'name' del módulo)
public_bp = Blueprint(
    'public',
    __name__,
    template_folder='templates',
)

from . import routes # importar todas las vistas del Blueprint
