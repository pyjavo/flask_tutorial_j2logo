#!/bin/bash

source venv/bin/activate

export FLASK_APP=entrypoint
export FLASK_DEBUG=True
export APP_SETTINGS_MODULE=config.local

flask run