from .default import *


# Deshabilita la captura de errores durante el manejo de peticiones
# para obtener mejores informes de error en los tests
TESTING = True

# Activa el modo debug
DEBUG = True

# Nombre del entorno de ejecución
APP_ENV = APP_ENV_TESTING

# Deshabilitar la protección CSRF durante los tests
WTF_CSRF_ENABLED = False

# Para los tests, usaremos una base de datos diferente a la de desarrollo
# Esta variable existe en instance/config-testing.py
# SQLALCHEMY_DATABASE_URI = 'sqlite:///project_test.db'
