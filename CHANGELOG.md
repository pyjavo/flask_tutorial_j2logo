# Changelog

Cambios notables realizados en esta versión del tutorial.


## [leccion5] - 2023-10-05

### Changed

- Hasta este punto se mantenía la estructura de carpetas simple. Luego de esto se hace la división en 3 diferentes apps, que son: `admin`, `auth` y `public`.

[leccion5]: https://gitlab.com/pyjavo/flask_tutorial_j2logo/-/tags/leccion5


## [leccion10] - 2023-10-17

### Added

- Punto previo antes de empezar a utilizar migraciones para la base de datos.

[leccion10]: https://gitlab.com/pyjavo/flask_tutorial_j2logo/-/tags/leccion10


## leccion12 - 2023-10-25

### Changed

- Cambie el uso de `User.query.get(id)` por `User.query.session.get(User, id)` en el método `get_by_email` de User porque aparecía un warning de que sería deprecado ese método ([`c34fe99e`](https://gitlab.com/pyjavo/flask_tutorial_j2logo/-/commit/b32efb1d26a1bbc995aed9c1a566374adacf9495)).


## datos dummy - 2023-11-04

### Added

- Agregue un comando por CLI para agregar N cantidad de articulos asociados a un usuario al azar de la base de datos. Esto con el fin de luego probar la paginación en la vista index. Los datos de prueba para los posts fueron generados con Faker ([`705cf26`](https://gitlab.com/pyjavo/flask_tutorial_j2logo/-/commit/705cf26e5e25017cc5624a03c5b60491d51be8a8)).


## [leccion14] - 2023-11-08

### Changed

- Notas sobre el el envio de correos. No lo implementé porque parece aburrido. Lo único interesante ahi es el uso de `threading` para enviarlo en un hilo diferente.

[leccion14]: https://gitlab.com/pyjavo/flask_tutorial_j2logo/-/commit/ae3b98970e73a763e2de5c7b8ee54931d9116756
