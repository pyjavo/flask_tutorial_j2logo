# Tutorial

Un resumen descafeinado del tutorial https://j2logo.com/tutorial-flask-espanol/

## Capítulo 1


- Dentro del patrón MVC, los métodos de las URLS corresponderían con el controlador.
- no nombrar al fichero que instancia nuestra aplicación «flask.py» ya que entraría en conflicto con el propio Flask
- Modificar `env/bin/activate`. Al final del fichero añadimos lo siguiente:
export FLASK_APP="run.py"
export FLASK_DEBUG=True


```python
@app.route('/hello')
def hello_world():
    return 'Hello, World!'

# run.py
@app.route("/")
def index():
    return "{} posts".format(len(posts))


@app.route("/p/<string:slug>/")
def show_post(slug):
    '''
    http://127.0.0.1:5000/p/mi-primera-entrada/
    '''
    return "Mostrando el post {}".format(slug)

@app.route("/admin/post/")
@app.route("/admin/post/<int:post_id>/")
def post_form(post_id=None):
    return "post_form {}".format(post_id)
```

- El proyecto corre con `flask run`


## Capítulo 2: plantillas
https://j2logo.com/tutorial-flask-leccion-2-plantillas/



https://github.com/j2logo/tutorial-flask
```git
git checkout tags/leccion1 -b leccion1
```

- Propósito: Desarrollar un miniblog
- Vistas: métodos de los endpoints
- a una URL le podemos añadir secciones variables o parametrizadas con <param>
    <converter:param>
    
    Ej: 
    `@app.route("/p/<string:slug>/")`
    - Flask redirigirá la URL a una acabada con "/"

- print(url_for("show_post", slug="leccion-1", preview=True))
- Flask convierte el valor que devuelve una vista en un objeto «respuesta» (o response) por ti.
    - se puede devolver una tupla con la forma (response, status, headers:list or dict)
- Jinja
    - convierte los corchetes `<p>hola j2logo</p>` en &gt; y &lt; para evitar inyecciones.
    - Disponible url_for() y variables globales: config, request, session y g.

- Estáticos: 
    - estos ficheros deben ser servidos por un servidor web como Apache o Nginx
    - no por un servidor de aplicaciones como gunicorn o el servidor que viene con Flask

- ¿Es buena práctica en Flask tener una sola vista con 2 endpoints para crear/modificar un post?
```python
@app.route("/admin/post/")
@app.route("/admin/post/<int:post_id>/")
def post_form(post_id=None):
    return render_template("admin/post_form.html", post_id=post_id)
```


## Capítulo 3: uso de formularios

Objetivos:
    - registrar usuarios invitados al blog
    - admin pueda crear entradas al blog

- Si el attr "action" del <form> se deja vacío, la URL será la misma desde la que se descargo el recurso.
- Identificar el nombre con el que el servidor identificará los campos
- El objeto request contiene toda la información que el cliente (por ej: el navegador web) envía al servidor
- Con request.args.get se acceden a attrs de la URL
    - /?page=1&lista=10
- request.form es un diccionario con los nombres de los inputs del form que fueron enviados
- Hacer redirect luego del envío para evitar enviar datos duplicados.


- Al usar Flask-WTF ---> https://flask-wtf.readthedocs.io/en/1.1.x/install/
    - todo formulario se representa a través de una clase
    - Esta clase hereda del objeto FlaskForm
- Para evitar que el navegador valide los campos por nosotros y delegar esta tarea a nuestra clase, hemos añadido el atributo novalidate a la etiqueta del formulario.
- La nueva plantilla html del formulario:
    - Espera un objeto de la clase SignupForm, que hemos instanciado en la vista correspondiente, y lo hemos llamado form.
    - Dicho objeto contiene los campos del formulario y sabe cómo renderizarlos.
    - Ataques CSRF ----> https://es.wikipedia.org/wiki/Cross-site_request_forgery
- Como generar claves secretas ----> https://j2logo.com/generar-api-key-secreto-python/
- validate_on_submit(). Este método comprueba por nosotros que se ha enviado el formulario y que todos sus campos son válidos.

- Prueba el formulario de creación de posts ----> http://127.0.0.1:5000/admin/post/


```python
# forms.py
class SignupForm(FlaskForm):
    name = StringField('Nombre', validators=[DataRequired(), Length(max=64)])
    password = PasswordField('Password', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Registrar')

# run.py
from forms import SignupForm
@app.route("/signup/", methods=["GET", "POST"])
def show_signup_form():
    form = SignupForm()
    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        password = form.password.data
        next = request.args.get('next', None)
        if next:
            return redirect(next)
        return redirect(url_for('index'))
    return render_template("signup_form.html", form=form)
```

## Capítulo 4: Login de usuarios


Objetivos:
    - Crear modelo de usuarios
    - Formulario de login
    - Proteger ciertas vistas de usuarios no autenticados

- Flask-login ----> https://pypi.org/project/Flask-Login/
    - Almacenar el ID del usuario en la sesión y mecanismos para hacer login y logout.
    - Restringir el acceso a ciertas vistas únicamente a los usuarios autenticados.
    - "Recuérdame", para mantener la sesión incluso después de que el usuario cierre el navegador.
    - Proteger el acceso a las cookies de sesión frente a terceros.

- Crear una instancia de la clase LoginManager
    - accesible desde cualquier punto de nuestra aplicación

- Crear clase User
    - Heredad de flask_login.UserMixin e implementar
        - is_authenticated: bool
        - is_active: bool
        - is_anonymous: bool
        - get_id(): string
    - Las contraseñas pasarán por una funcion hash para generar un string, aleatorio y único

- Decorador de flask_login en endpoint
    - @login_manager.user_loader para el listado de usuarios

- Proceso para autenticar usuarios
    - clase formulario
    - plantilla HTML
    - vista para el login

- Variables o métodos de Flask_login usados en las vistas:
    - login_user, current_user y @login_required


## Capítulo 5: Añadiendo una base de datos con SQLAlchemy

- Un ORM (Object-Relational Mapper) es un toolkit que nos ayuda a trabajar con las tablas de la base de datos (BD) como si fueran objetos.
    - De manera que cada tabla se mapea con una clase y cada columna con un campo de dicha clase.
    - También nos permite mapear las relaciones entre tablas como relaciones entre objetos.
- Instalar Flask-SQLAlchemy ----> https://pypi.org/project/Flask-SQLAlchemy/
- El tutorial usará PostgreSQL. Yo usaré SQLite por simpleza
    - [psycopg2](https://pypi.org/project/psycopg2/) es un adaptador PostgreSQL para Python implementado usando [libpq](https://www.postgresql.org/docs/current/libpq.html)
    - https://www.psycopg.org/

- SQLALCHEMY_TRACK_MODIFICATIONS viene deshabilitado por default desde la version 3.0
    - [Fuente](https://flask-sqlalchemy.palletsprojects.com/en/3.1.x/config/#flask_sqlalchemy.config.SQLALCHEMY_TRACK_MODIFICATIONS): `app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False` 
- Actualizamos el modelo User
    - Se hereda de db.Model
    - Se quita el método __init__ para empezar a usar atributos de tipo db.Column()
    - En la versión 3.1 [los modelos](https://flask-sqlalchemy.palletsprojects.com/en/3.1.x/models/#defining-models) usan `mapped_column` y soportan el tipado de los datos con `Mapped`. Igualmente esto generará un objeto `Table` con objetos `Column()`
    - Se elimina la lista users y la función get_user
    - `save()`: el objeto debe estar previamente asociado al objeto session. El commit reflejará los cambios en la BD.
    - Los objetos que heredan de `Model` tiene el objeto `query` para realizar consultas.
    - Se actualizan las vistas login, load_user y show_signup_form para que usen `User` y sus métodos.
    - Se actualiza la plantilla *signup_form.html* para mostrar error si el email de usuario ya está en uso.
- Se crea un modelo `Post`
    - Se instala python-slugify ---> https://pypi.org/project/python-slugify/
    - relacionado con el modelo `User` por una llave foránea.
    - El método `save` valida si hay slugs únicos para los títulos
    - En `run.py` se importa [abort](https://flask.palletsprojects.com/en/2.3.x/api/#flask.abort) de flask
    - Se actualizan las vistas index, show_post y post_form para usar `Post` (asi mismo las plantillas)
    - Se borran la referencia a `title_slug` en el formulario PostForm

- Crear la BD siguiendo esta respuesta de [SO](https://stackoverflow.com/a/73962250/2142093)
Debe agregarse [el contexto de la aplicación](https://flask-sqlalchemy.palletsprojects.com/en/3.1.x/contexts/)

```python
>>> from run import db, app
>>> app.app_context().push()
>>> db.create_all()
```

## Capítulo 6: Estructura de un proyecto con Flask: blueprints


- Usualmente se usa el fichero `app.py` (en este caso `run.py`) para contener todo el código de la app. Pero no es práctico para todos los casos, porque debe incluir mucho código:
    - Inicialización de la app
    - Inicialización de la extensiones
    - Las vistas (los endpoints)
    - Los modelos
    - Los formularios
    - Métodos para crear la BD
- Inicialmente, la estructura que llevamos es:
```
+ mi_proyecto
|_ app.py
|_ forms.py
|_ models.py
|_ static/
|_ templates/
```
- Se propone dividir `app.py` en varios módulos, donde cada uno agrupe funcionalidades.
    - Gestión de usuarios.
    - Parte pública.
    - Parte privada.
    - Panel de administración.
    - Envío de emails.
    - Envío de notificaciones push.
    - API para las aplicaciones móviles.
    - Ejecución de tareas en background.

- **División estructural** (agrupar el código por lo que hace)
```
+ mi_proyecto/
   |_ app/
      |_ __init__.py
      |_ static/
      |_ templates/
         |_ public/
            |_ index.html
            |_ ...
         |_ users/
            |_ login.html
            |_ sign_up.html
            |_ ...
         |_ private/
            |_ index.html
         |_ ...
      |_ routes/
         |_ __init__.py
         |_ private.py
         |_ public.py
         |_ users.py
         |_ ...
      |_ models/
         |_ users.py
         |_ ...
      |_ forms/
         |_ users.py
         |_ ...
   |_ run.py
   |_ requirements.txt
   |_ ...
```
- **División funcional** (agrupar los distintos componentes según los requisitos funcionales)
```
+ mi_proyecto/
   |_ app/
      |_ __init__.py
      |_ public/
         |_ __init__.py
         |_ routes.py
         |_ static/
         |_ templates/
         |_ models.py
         |_ forms.py
         |_ ...
      |_ private/
         |_ __init__.py
         |_ routes.py
         |_ static/
         |_ templates/
         |_ models.py
         |_ forms.py
         |_ ...
      |_ users/
         |_ __init__.py
         |_ routes.py
         |_ static/
         |_ templates/
         |_ models.py
         |_ forms.py
         |_ ...
      |_ ...
      |_ static/
      |_ templates/
   |_ run.py
   |_ requirements.txt
   |_ ...
```

- **Blueprints**
    - Define una colección de:
        - vistas
        - plantillas
        - recursos estáticos
        - modelos,
        - etc. que pueden ser utilizados por la aplicación
    - Los usaremos siempre y cuando queramos organizar la aplicación en diferentes módulos.

- Pasos para usar Blueprints
    1. Creación del nuevo objeto Blueprint con su nombre específico su correspondiente fichero `__init__.py`
    ```python
    from flask import Blueprint
    public = Blueprint(
        'public',
        __name__,
        template_folder='templates',
        static_folder='static'
    )
    ```
    2. Importar el archivo `routes` (paso 3) en `__init__.py`
    ```python
    from . import routes
    ```
    3. Crear el archivo `routes.py` con las rutas o endpoints.
        - El decorador `route` para definir las URLs de las vistas se toma del objeto `Blueprint` ya creado y no de la app (antes se escribían `@app.route("/")`).
    4. Registro del Blueprint en la app específica
    ```python
    from flask import Flask
    app = Flask(__name__)
    from .public import public
    app.register_blueprint(public)
    ```

- Estructura que sigue j2logo para sus proyectos Flask
```bash
+ mi_proyecto/
   |_ app/
      |_ __init__.py
      |_ common/
      |_ modulo1/
         |_ __init__.py
         |_ routes.py
         |_ templates/
            |_ modulo1/
               |_ template1.html
               |_ template2.html
         |_ models.py
         |_ forms.py
         |_ ...
      |_ modulo2/
         |_ __init__.py
         |_ routes.py
         |_ templates/
            |_ modulo2/
               |_ template1.html
               |_ template2.html
         |_ models.py
         |_ forms.py
         |_ ...
      |_ ...
      |_ static/
         |_ css/
         |_ images/
         |_ js/
      |_ templates/
         |_ base_template.html
         |_ ...
   |_ config
      |_ development.py
      |_ local.py
      |_ production.py
      |_ testing.py
   |_ env/
   |_ fixtures/
   |_ instance
      |_ __init__.py
      |_ config.py
   |_ .gitignore
   |_ CHANGELOG.md
   |_ entrypoint.py
   |_ README.md
   |_ requirements.txt
```
Ficheros:
- El directorio `app/` es el paquete de Python donde reside toda la aplicación de Flask.
- El directorio `app/common` tiene contiene código en común para toda la aplicación (ej: cálculo de zonas horarias, conversión de monedas)
- Los nombres `modulo1` y `modulo2` pueden ser nombres de Blueprints que hayas creado (ej: usuarios, publicaciones, ingresos, gastos, etc).
- CHANGELOG.md se registra las funcionalidades y corrección de errores de cada versión.
- La entrada a la app sería `entrypoint.py` dónde se crea crea la aplicación y se lanza el servidor de desarrollo.
- El directorio `config/` contiene módulos con las variables de configuración de cada uno de los entornos de ejecución (local, test, staging, production. Más en la lección 7)
- El directorio `env/` contendrá el entorno de ejecución del proyecto
- El directorio de Flask `instance/` contiene variables de configuración del entorno local y que se ignorarán por git. Ej: contraseñas personales.
- El directorio `fixtures/` contiene recursos usados durante el desarrollo (archivos CSV, SQL o datos de prueba para la API).

- Reorganizar el miniblog en Blueprints para dividir funcionalidades
    - Mover el contenido de `run.py`, `models.py` y `forms.py` a la nueva estructura.
    - División funcional para 3 módulos:
        - admin, para la gestión del blog.
        - auth, para la gestión de los usuarios.
        - public, para todo lo relacionado con la parte pública del blog.

```
+ proyecto_miniblog
|_ app
   |_ __init__.py
   |_ models.py
   |_ admin/
      |_ __init__.py
      |_ forms.py
      |_ routes.py
      |_ templates/
         |_ admin/
            |_ post_form.html
   |_ auth/
      |_ __init__.py
      |_ forms.py
      |_ models.py
      |_ routes.py
      |_ templates/
         |_ auth/
            |_ login_form.html
            |_ signup_form.html
   |_ public/
      |_ __init__.py
      |_ routes.py
      |_ templates/
         |_ public
            |_ index.html
            |_ post_view.html
   |_ static/
      |_ base.css
   |_ templates/
      |_ base_template.html
|_ env/
|_ .gitignore
|_ CHANGELOG.md
|_ entrypoint.py
|_ LICENSE
|_ README.md
|_ requirements.txt
```

### Correr el proyecto

- Quitar las variables de entorno del `venv` y crear un archivo de bash (Linux/Mac) que exporte las variables de configuración que se usarán en el **capítulo 7**.


El archivo se llamará `archivo.sh`

```bash
#!/bin/bash

source venv/bin/activate

export FLASK_APP=entrypoint
export FLASK_DEBUG=True
export APP_SETTINGS_MODULE=config.local

flask run
```
- Crear la base de datos desde una consola de Python

```python
from entrypoint import app
app.app_context().push()
from app import db
db.create_all()
```

El proyecto ahora deberá ejecutarse de la siguiente forma:
```bash
source archivo.sh
```

## Capítulo 7: Parámetros de configuración de un proyecto Flask

### Introducción
Los parámetros de configuración de un proyecto nos permiten definir:
- la cadena de conexión a la base de datos
- la duración de la cookie de sesión
- el idioma por defecto de la aplicación
- entre otros

Son accesibles en cualquier parte del código (como si fueran variables globales).

Ejemplos:
- SECRET_KEY
- SQLALCHEMY_DATABASE_URI
- SQLALCHEMY_TRACK_MODIFICATIONS

### Parámetros de configuración según el entorno de ejecución
- Lo usuar es definir los parámetros de configuración en un fichero independiente de tu código.

### Principales parámetros de configuración de Flask

Crear ficheros, uno por entorno, para definir parámetros de configuración:
- ENV: Indica entornos `development` y `production`.
- DEBUG: Reinicia El servidor de desarrollo si detecta cualquier cambio en el código.
- TESTING: Habilita el modo de test.
- SECRET_KEY: firma la cookie de sesión, entre otros aspectos de seguridad.



### El objeto config en Flask

- El objeto **config** es realmente una subclase de la clase dictionary. Por tanto, puede manipularse como un diccionario cualquiera.

### Técnicas para separar los parámetros de configuración en función del entorno
Tipos de entornos:

- Local: Es tu propio entorno, tu ordenador, donde desarrollas el código.
- Desarrollo: Es un entorno compartido en el que todos los programadores tienen acceso. Se suele usar para probar los cambios que se están haciendo durante el desarrollo de la aplicación.
- Staging: Es el entorno de preproducción. Un entorno de pruebas lo más parecido posible a producción. Generalmente es un entorno estable, sin fallos, que se suele usar para que los clientes puedan probar la aplicación de forma independiente al entorno de desarrollo.
- Test: Es el entorno en el que se ejecutan los diferentes test.
- Producción: Este es el entorno real donde se despliega la aplicación para su uso.

#### Usando herencia
- La primera opción (que no es mi favorita), es definir un fichero de configuración llamado `config.py` que lucirá así:

```python
# Fichero de configuración config.py

class Config(object):
    SECRET_KEY = '7110c8ae51a4b5af97be6534caef90e4bb9bdcb3380af008f90b23a5d1616bf319bc298105da20fe'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://db_user:db_pass@prod_host:port/db_name'

class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://db_user:db_pass@dev_host:port/db_name'

class StagingConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://db_user:db_pass@staging_host:port/db_name'

class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://db_user:db_pass@test_host:port/db_name'
```
Luego para cargar una de estas configuraciones:

```python
def create_app(settings_module='config.DevelopmentConfig'):
    app = Flask(__name__)
    app.config.from_object(settings_module)
```

Y por último, `settings_module` debe tener un valor que leemos de una variable de entorno:

```python
# Fichero entrypoint.py
# Por ejemplo, la variable de entorno APP_SETTINGS_MODULE = config.ProductionConfig

import os
from app import create_app

settings_module = os.getenv('APP_SETTINGS_MODULE')
app = create_app(settings_module)
```


#### Usando ficheros independientes, uno por entorno ✅

Lo mejor es rener un fichero por cada entorno:
- config/default.py
- config/prod.py
- config/dev.py

Modificar `app/__init__.py` cargar la configuración específica y crear la variable de entorno `APP_SETTINGS_MODULE`

### El directorio Instance

Flask pone a nuestra disposición un directorio especial, llamado **instance**, que podemos utilizar para crear ficheros con parámetros de configuración que no queremos que formen parte del sistema de control de versiones.

Aquí podemos poner la cadena de conexión a la base de datos local

- Se debe crear un archivo `instance/config.py` con los parámetros de configuración
- Se importan los valores de **instance** en el `app/__init__.py` de esta forma

```python
app.config.from_pyfile('config.py', silent=True)
```

### Consejos prácticos

- Separa la configuración de tu aplicación con diferentes ficheros, uno por entorno.
- Indica el fichero de configuración a utilizar con una variable de entorno.
- Usa el directorio `instance` para definir aquellos parámetros que no deben subirse a github/gitlab.
- Define las variables de entorno `FLASK_ENV` y `FLASK_DEBUG` en lugar de usar los parámetros ENV y DEBUG, respectivamente.
- Recuerda que lo ideal es cargar todos los parámetros de configuración antes de arrancar la aplicación (usando por ejemplo el método `create_app`).
- No escribas código que necesite de parámetros de configuración en tiempo de importación. Si fuera necesario, reescribe el código para que pueda usarlos en un momento posterior.
- En mi caso, los valores `development` y `production` que puede tener el parámetro `ENV` son insuficientes. Yo suelo declarar un parámetro `APP_ENV` con más valores, uno por entorno. Esto me sirve por si necesito conocer desde el código de la aplicación el entorno en el que se está ejecutando.

### Un ejemplo práctico

- Aplicar la lección 7 al miniblog. Se modigica `create_app`:
    - Se establece en el parámetro `instance_relative_config` a `True` para que tenga en cuenta que el directorio `/instance` se encuentra al mismo nivel que el directorio `/app`.
    - Carga el archivo config especificado por la variable de entorno APP.
    - Carga la configuración de la carpeta instance
- Modificar el `entrypoint` para crear la variable settings_module`
- Crear los directorios `config`
    - Crear config/default.py
    - Crear config/dev.py
    - Crear config/local.py
    - Crear config/prod.py
    - Crear config/staging.py
    - Crear config/testing.py

- Crear los directorios `instance`
    - Crear instance/config.py
        ```python
        SQLALCHEMY_DATABASE_URI = 'sqlite:///project.db'
        ```
    - Crear instance/config-testing.py
        ```python
        SQLALCHEMY_DATABASE_URI = 'sqlite:///project_test.db'
        ```
- env??? ---> yo estoy usando el `archivo.sh` para crear las variables de entorno.


Recuerda que el proyecto se ejecuta así:
```bash
source archivo.sh
```

## Capítulo 8: Gestión de errores

El ideal es que la app sea lo más robusta posible, es decir, que al usuario se le presenten el mínimo número de errores.

Errores típicos:
- **Errores de programación:** El error más típico es que el código tenga un bug.
- **Errores al escribir en un fichero:** el fichero ya existe, no hay permisos de escritura en un directorio, …
- **Errores de base de datos:** de integridad, permisos, fallos de conexión, …
- **Errores al conectar con un servicio externo:** permisos de acceso, indisponibilidad, errores de conexión, …

### ¿Qué sucede cuando hay un error no controlado?

- Se le muestra una página de error al usuario. Se sirven de acuerdo al estado devuelto por la respuesta de la aplicación. Los códigos de estado HTTP más comunes son:
    - **200 ~299:** Respuestas satisfactorias.
    - **400 ~ 499:** Hubo un problema con la petición. Normalmente relacionados con el cliente o el usuario.
    - **500 ~ 599:** Hubo un problema en la parte del servidor. Suelen ser fallos en el código.
Lee más del tema en la documentación de Mozilla sobre [códigos de estado de respuesta HTTP](https://developer.mozilla.org/es/docs/Web/HTTP/Status)


- Ejemplos:
    - Si el usuario ingresa a una URL que no hemos creado, se le mostrará un error 404 "Not Found". Por ejemplo, al acceder a `http://localhost:5000/contacto`
    - Si el usuario ingresa a una URL cuya vista tiene un error, por ejemplo, una división por cero, entonces se le mostrará al usuario la página de error 500
        - con modo DEBUG activo se nos mostrará como título la excepción de Python `ZeroDivisionError` y el traceback para poder hacer debuging
        - Sin modo DEBUG, el usuario debería ver la página con el título "Internal Server Error". La idea es que el usuario no vea el traceback ni las rutas de archivos en nuestro servidor.

- La función `abort()` de Flask detiene la ejecución de la petición de forma prematura y devuelve un código de estado HTTP permitido (el que le pasemos como parámetro).

```python

# ejemplo en public/routes.py

@public_bp.route("/p/<string:slug>/")
def show_post(slug):
    post = Post.get_by_slug(slug)
    if post is None:
        abort(404)
    return render_template("public/post_view.html", post=post)
```

### Manejadores de errores
- Personalizar las páginas de error a través de los manejadores de errores.
- Los manejadores de errores son funciones que se ejecutan cuando se produce una situación de error.

Hay dos formas de definir un manejador de error:

- Decorando una función con `errorhandler()`
- Registrando una función como:
```python
errorhandler: app.register_error_handler(codigo_estado, funcion)
```
- Se pueden manejar dos tipos de errores:
    - Códigos de estado HTTP conocidos
    - Excepciones
        - También es posible lanzar una excepción y asociarla a un manejador de error.

**Páginas de error personalizadas**
- Agrega al final de `app/__init__.py` una nueva función

```python
from flask import render_template

def register_error_handlers(app):
    '''Manejadores de errores personalizados.'''

    @app.errorhandler(500)
    def base_error_handler(e):
        return render_template('500.html'), 500

    @app.errorhandler(404)
    def error_404_handler(e):
        return render_template('404.html'), 404
```
- Al final de `create_app` agrega lo siguiente, justo antes del `return app`

```python
    # Custom error handlers
    register_error_handlers(app)

    return app
```

- Crea un fichero con nombre `404.html` y otro llamado `500.html` en el directorio `app/templates/`

```html
<!-- archivo 404.html -->

{% extends 'base_template.html' %}
{% block content %}
    Ooops!! La página que buscas no existe xD
{% endblock %}
```

```html
<!-- archivo 500.html -->

{% extends 'base_template.html' %}
{% block content %}
    Ooops!! Parece que ha habido un error :(
{% endblock %}
```

- Cambiamos `abort` en el endpoint `show_post()` por la excepción `NotFound` de `werkzeug` para que nuestro nuevo manejador de errores 404 maneje el error cuando se acceda a un post que no existe.
    - `NotFound` que hereda de `werkzeug.exceptions.HTTPException`

## Capítulo 9: Logs en Flask

### Configuración por defecto de los logs en Flask

- Flask, por defecto, usa el módulo [logging](https://docs.python.org/3/library/logging.html) de la librería estándar de Python para los mensajes de log.

- Ejemplo de como imprimir un mensaje de log de carácter informativo

```python
# Fichero app/public/routes.py

from flask import render_template, current_app
# ...
# Otras importaciones

@public_bp.route("/")
def index():
    '''Vista de inicio del miniblog.'''

    current_app.logger.info('Mostrando los posts del blog')
    posts = Post.get_all()
    return render_template("public/index.html", posts=posts)
```

Cuando corras el servidor local, verás el mismo mensaje como parte de los logs que genera el servidor cuando accedas a `http://localhost:5000/`

```bash
 * Serving Flask app 'entrypoint'
 * Debug mode: on
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:5000
Press CTRL+C to quit
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 123-664-180
[12/10/2023 11:12:29.971]    INFO   [app.index:12]   Mostrando los posts del blog
127.0.0.1 - - [12/Oct/2023 11:12:29] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [12/Oct/2023 11:12:30] "GET /static/base.css HTTP/1.1" 304 -
```

### Mejorando los logs de Flask

**Repaso al módulo logging**

- Para poder escribir mensajes de log con el módulo `logging` necesitamos definir en nuestra aplicación, al menos, un objeto `logger`.
    - También podemos definir más de un `logger` por aplicación.
    - Al crear un objeto `logger` debemos especificar un nombre. Este nombre tiene una nomenclatura jerárquica que se establece en base al carácter `'.'`. Es decir, podemos tener un logger llamado `app` y otro que sea un hijo de este llamado `app.public.routes`.
    - A un `logger` se le pueden asociar manejadores (o handlers), que indican:
        - Qué hacer con el mensaje
        - Para qué nivel de log se activan.


**Niveles de los logs:**

Los niveles tienen un orden de gravedad de menor a mayor, como se muestran a continuación:

1. **DEBUG:** Para depurar información. Nivel de información muy detallado.
2. **INFO:** Para mensajes informativos que indican que la app se está ejecutando correctamente.
3. **WARNING:** Para mensajes de advertencia cuando la app, aún funcionando correctamente, detecta una situación inesperada o posible problema futuro.
4. **ERROR:** Para mensajes de error.
5. **EXCEPTION:** Para mensajes de error que se producen debido a una excepción. Se muestra la traza de la excepción.

A tener en cuenta:

- Así que cuando un handler se configura para el nivel `DEBUG`, además de mostrar los mensajes de `DEBUG` también mostrará los de `INFO` o de `ERROR`, puesto que tienen un nivel de gravedad mayor.
- Si un handler se configura con nivel `ERROR`, este no procesará los mensajes que se escriben en el nivel de `DEBUG` dado que son menos graves.


**Modificando la configuración del logger por defecto de Flask**
- Abre `app/__init__.py` y agrega la siguiente función al final:

```python
import logging

def configure_logging(app):
    '''Configurar el módulo logging.'''

    # Eliminamos los posibles manejadores, si existen, del logger por defecto
    del app.logger.handlers[:]

    # Añadimos el logger por defecto a la lista de loggers.
    # De momento, solo el logger por defecto de Flask
    loggers = [app.logger, ]
    handlers = []

    # Creamos un manejador para escribir los mensajes por consola
    console_handler = logging.StreamHandler() # para escribir los mensajes por consola
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(verbose_formatter())
    handlers.append(console_handler)

    # Asociamos cada uno de los handlers a cada uno de los loggers
    for l in loggers:
        for handler in handlers:
            l.addHandler(handler)
        # Con propagate=False será el último logger en comprobar la existencia de handlers
        # y no buscará entre sus ancestros si los tuviera.
        l.propagate = False
        l.setLevel(logging.DEBUG)
```

Ahora creamos el formateador que hemos creado para nuetros mensajes. Este formateador registra:
- la fecha en que se escribe el mensaje
- el nivel de gravedad
- el nombre del logger que lo escribe
- la función/método dónde se escribe
- la línea de código
- el propio texto del mensaje.

```python
def verbose_formatter():
    return logging.Formatter(
        '[%(asctime)s.%(msecs)d]\t %(levelname)s \t[%(name)s.%(funcName)s:%(lineno)d]\t %(message)s',
        datefmt='%d/%m/%Y %H:%M:%S'
    )
```
- Se añade la llamada a `configure_logging()` dentro de la función `create_app()` justo después de cargar la configuración
```python
    # ... el código anterior

    # Carga la configuración de la carpeta instance
    if app.config.get('TESTING', False):
        app.config.from_pyfile('config-testing.py', silent=True)
    else:
        app.config.from_pyfile('config.py', silent=True)

    configure_logging(app)

    # ... el código siguiente
```

Para hacer la prueba de que funciona, veamos nuevamente `http://localhost:5000/` para observar el mensaje que configuramos en el index.


**Definiendo una configuración para cada entorno de ejecución**

- Veamos cómo podemos establecer una configuración distinta de los logs en Flask para cada entorno de ejecución. Simplemente vamos a añadir unas cuantas líneas de código a la función `configure_logging()`.

- Abre el fichero `app/__init__.py` y actualiza la función `configure_logging()` como te indico a continuación:

```python
def configure_logging(app):
    '''Configurar el módulo logging.'''

    # Eliminamos los posibles manejadores, si existen, del logger por defecto
    del app.logger.handlers[:]

    # Añadimos el logger por defecto a la lista de loggers
    loggers = [app.logger, ]
    handlers = []

    # Creamos un manejador para escribir los mensajes por consola
    console_handler = logging.StreamHandler()
    #console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(verbose_formatter())
    #handlers.append(console_handler)

    if (app.config['APP_ENV'] == app.config['APP_ENV_LOCAL']) or (
            app.config['APP_ENV'] == app.config['APP_ENV_TESTING']) or (
            app.config['APP_ENV'] == app.config['APP_ENV_DEVELOPMENT']):
        console_handler.setLevel(logging.DEBUG)
        handlers.append(console_handler)
    elif app.config['APP_ENV'] == app.config['APP_ENV_PRODUCTION']:
        console_handler.setLevel(logging.INFO)
        handlers.append(console_handler)

    # Asociamos cada uno de los handlers a cada uno de los loggers
    for l in loggers:
        for handler in handlers:
            l.addHandler(handler)
        l.propagate = False
        l.setLevel(logging.DEBUG)
```

- Con esto se logra que:
    - Si estamos ejecutando la aplicación en el entorno de producción, solo se mostrarán los mensajes con un nivel de gravedad de `INFO` en adelante (no se mostrarán los mensajes de `DEBUG`).
    - Si estamos en los entornos local, test o desarrollo, sí se mostrarán los mensajes de `DEBUG`.

### Envío de errores al administrador

- Se creará un nuevo handler para enviar los mensajes de error al correo. Se añadirá un par de líneas al `configure_logging()` dentro de la configuración del entorno de producción:


```python
from logging.handlers import SMTPHandler

mail_handler = SMTPHandler((app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
                            app.config['DONT_REPLY_FROM_EMAIL'],
                            app.config['ADMINS'],
                            '[Error][{}] La aplicación falló'.format(app.config['APP_ENV']),
                            (app.config['MAIL_USERNAME'],
                            app.config['MAIL_PASSWORD']),
                            ())
mail_handler.setLevel(logging.ERROR)
mail_handler.setFormatter(mail_handler_formatter())
handlers.append(mail_handler)
```

- A continuación, se añade la nueva función `mail_handler_formatter()`.
- Por último, en `config/default.py` colocamos los parámetros para configurar el smtp

```python
# Configuración del email
MAIL_SERVER = 'tu servidor smtp'
MAIL_PORT = 587
MAIL_USERNAME = 'tu correo'
MAIL_PASSWORD = 'tu contraseña'
DONT_REPLY_FROM_EMAIL = 'dirección from'
ADMINS = ('javier@example.com', )
MAIL_USE_TLS = True
MAIL_DEBUG = False
```

### Integrando extensiones y otros módulos

- Añade los loggers de los paquetes de Python que instales.
- Ejemplo con `aqlalchemy`

```python
# ficher app/__init__.py

loggers = [app.logger, logging.getLogger('sqlalchemy'), ]
```

Para más info, ver https://docs.sqlalchemy.org/en/20/core/engines.html#configuring-logging

- Se puede crear un logger por cada módulo que usará la app para saber cuál genera el error.


- Cambiaré el logger de sqlalchemy para que solo me muestre mensajes de error y no tener mucho texto en mi consola

```python
# ficher app/__init__.py

loggers = [app.logger, logging.getLogger('sqlalchemy').setLevel(logging.ERROR), ]
```
