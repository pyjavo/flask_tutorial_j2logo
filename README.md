# Tutorial de Flask

En los archivos:
- [notas.md](notas.md), (Lecciones 1-9).
- [notas2.md](notas2.md), (Lecciones 10-17).

Encontrarás un resumen descafeinado del tutorial https://j2logo.com/tutorial-flask-espanol/

El repositorio original del tutorial se encuentra en https://github.com/j2logo/tutorial-flask

## ¿Cómo correr el proyecto?

1. Darle permiso de ejecución al archivo `archivo.sh`

```bash
chmod 744 archivo.sh
```

*Para Windows, debes investigar como hacerlo con archivos .bat*

2. Crear un entorno virtual `venv` y activarlo
3. Instalar las dependencias en tu entorno virtual con `pip install -r requirements.txt`
4. Ejecutar el archivo desde tu máquina `source archivo.sh`


## ¿Cómo contribuir?
Cualquier sugerencia para mejorar será bien recibida 😁. Lee el archivo [CONTRIBUTING.md](https://gitlab.com/pyjavo/flask_tutorial_j2logo/-/blob/main/CONTRIBUTING.md)


## Recursos

### Guía para el [changelog](https://gitlab.com/pyjavo/flask_tutorial_j2logo/-/blob/main/CHANGELOG.md)
- https://common-changelog.org/ (guía general del formato)
- https://keepachangelog.com/en/1.0.0/ (tipos de cambios)

### Guía de estilos
- https://google.github.io/styleguide/pyguide.html


## TODO:
- Borrar datos del formulario una vez enviado.
- Usar una plantilla HTML con diseño CSS.
