## Capítulo 10: Añadiendo seguridad en las vistas

- Solo ciertos roles puedan realizar determinadas acciones. Por ejemplo, los admins son los únicos que pueden:
    - Crear entradas de blog
    - Borras las entradas del blog
    - Eliminar las entradas del blog
    - Consultar los usuarios registrados
    - Asignar a usuarios el rol de administrador

- Por ello, esta lección del tutorial se centrará en el paquete `app.admin`

### La forma menos buena de añadir seguridad en las vistas

- Agregar `if current_user.is_admin` en el código, pero es muy engorroso porque son varias funcionalidades las que hay que proteger.

### Añadiendo seguridad en las vistas con decoradores

- El uso de decoradores resuelve el problema anterior. Se creará uno llamado `@admin_required`.
- Resultan muy útiles para:
    - eliminar código repetitivo
    - separar la responsabilidad del código
    - añadir seguridad a las vistas
    - aumentar su legibilidad.

Ejemplo de un decorador sencillo
```python
from functools import wraps

def print_hola(f):
    @wraps(f)
    def decorated_function(*args, **kws):
        print('Hola')
        return f(*args, **kws)
    return decorated_function
@print_hola
def print_nombre(nombre):
    print(nombre)
>>> print_nombre('j2logo')
Hola
j2logo
```

**Ahora creemos nuestro propio decorador @admin_required**

- Crea un nuevo fichero llamado `decorators.py` en el directorio app/auth`
```python
from functools import wraps
from flask import abort
from flask_login import current_user

def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kws):
        is_admin = getattr(current_user, 'is_admin', False)
        if not is_admin:
            abort(401)
        return f(*args, **kws)
    return decorated_function
```

- Agreguemos el decorador encima de `@login_required` en la vista `post_form`.
    - Estamos concatenando las funcionalidades de ambos decoradores.
    - Antes de ejecutar la vista `post_form()` se comprueba si el usuario está autenticado y si es administrador.

```python
from app.auth.decorators import admin_required

# ... código de las rutas
@login_required
@admin_required
def post_form(post_id):
    # ... resto del código
```

- Si visitas `http://localhost:5000/admin/post/` y te aparece **Unauthorized** quiere decir que tuvimos éxito.


- Probemos con un nuevo usuario de administrador. Hay múltiples opciones para probar este escenario:

    - **Opción 1:** Registremos un usuario administrador por el formulario de registro y por base de datos cambiemos el valor de is_admin a 1 (equivale a `True` en python).
        - email `admin@example.com` y password `p0987654`
        - Aquí suponemos que el usuario tiene `id=2`. Averigua eso primero antes de actualizar.

        ```SQL
        UPDATE blog_user
        SET is_admin=TRUE
        WHERE id=2;
        ```
    - **Opción 2:** Crear el usuario por base de datos como dice el tutorial
        - Aquí la contraseña es `1234`
        ```SQL
        INSERT INTO blog_user(name, email, password, is_admin)
        VALUES ('ADMIN 2', 'admin@asd.com', 'pbkdf2:sha256:600000$fLJAo7qRQHzUNSHz$42d01c29a5651f64a1506583053849012cf8798e8658995ac73e341b2e89f373', TRUE);
        ```
        - Si quieres usar otra contraseña que no sea `1234` simplemente genera un nuevo hash en la consola de Python de la siguiente forma:
        ```python
        from werkzeug.security import generate_password_hash, check_password_hash
        password = '1234'
        given_password = generate_password_hash(password)
        check_password_hash(given_password, password)
        ```
    - Ahora comprueba que ya tienes acceso a la vista `post_form()`.

### Mejorando la parte de administración del blog

- Se crea página de error `401.html`
- Añadir un nuevo `@app.errorhandler(401)` para 401 en `app/__init__.py`

- Se añaden 2 nuevos métodos a la clase `User`
    - delete
    - get_all

- Se añaden 2 nuevos métodos a la clase `Post`
    - delete
    - get_by_id

- Tambien se elimina de `Post` porque depende de otro paquete
```python
def public_url(self):
    '''Obtener la url pública del post a partir del slug'''

    return url_for('public.show_post', slug=self.title_slug)
```
- En `app/public/templates/public/index.html` se borra el llamado a `public_url` y se cambia por
```html
<li>
    <a href="{{ url_for('public.show_post', slug=post.title_slug) }}">{{ post.title }}</a>
</li>
```
- Agregar la nueva lógica en `app/admin/routes.py` para:
    - Listar entradas del blog desde el punto de vista del administrador.
    - Se separa `post_form` en 2 vistas. Una para crear y otra para actualizar.
    - Se crea la vista para borrar una entrada del blog
    - Vista para listar los usuarios registrados
    - Vista para actualizar un usuario
    - Vista para eliminar a un usuario

- Se modifica el html para ajustarse a los cambios anteriores.
    - También se añade un enlace en la plantilla base para que sea fácil entrar a la sección del administrador


## Capítulo 11: Actualizar la base de datos con SQLAlchemy

¿Qué ocurre si ahora queremos añadir nuevos campos a nuestra tabla de Post?

Si agregamos nuevos atributos (ej: campo created`) al modelo `Post` y corremos nuestro código, podemos obtener un error de tipo

```bash
sqlalchemy.exc.OperationalError: (sqlite3.OperationalError) no such column: post.created
```
Una opción es añadir el campo a mano con SQL
```SQL
Alter table post add column created TIMESTAMP;
```

Pero si la nueva característica repercute múltiples modelos y tablas, se vuelve poco práctico este enfoque.

### Flask-Migrate para actualizar la base de datos
- Flask-Migrate ---> https://pypi.org/project/Flask-Migrate/

- Es una extensión basada en [Alembic](https://alembic.sqlalchemy.org/en/latest/) que se utiliza para llevar a cabo migraciones de bases de datos cuando usas SQLAlchemy como ORM.

- La recomendación es usarla desde el principio del proyecto.
- Para ver el paquete en acción, tendremos que partir de cero.
    - Comenta el nuevo campo que agregaste (ej: `created`)
    - Borra el archivo de sqlite (en la carpeta de instance).
    - Instalar el paquete `pip install flask-migrate`

### Configuración Flask-Migrate

Abre el fichero `app/__init__.py` y añade:

```python
from flask_migrate import Migrate

# Resto del código ...
db = SQLAlchemy()
migrate = Migrate()  # Se crea un objeto de tipo Migrate

# Resto del código ...
db.init_app(app)
migrate.init_app(app, db)  # Se inicializa el objeto migrate

# Resto del código ...
```

**Comandos principales**
- `flask db init`: Se ejecuta al principio. Crea una estructura de directorios y ficheros dentro de `/migrations`.
- `flask db migrate`: Busca actualizaciones en los modelos y genera los ficheros de migración de base de datos con los cambios detectados
- `flask db upgrade: Lleva a cabo la migración de la base de datos

**Uso paso a paso**
1. Ejecuta `flask db init` para crear directorio `/migrations`
2. Ejecuta `flask db migrate -m "Initial database"` para crear las tablas.
    - Se genera un nuevo fichero python que incluye los cambios para actualizar la BD. Se guarda en `migrations/versions`
    - Esto crea una tabla que se llama `alembic_version` cuyo registro tiene el número del hash que tiene como nombre el fichero de la migración.
3. Ejecuta `flask db upgrade` para que se creen estas tablas


### Actualizando el modelo Post con Flask-Migrate

- Ahora la idea es añadir un nuevo campo `created` al modelo Post que queremos que se vea reflejado en la BD. Para ello haremos:

`flask db migrate -m "Nuevo campo created a Post"`

- Por último se lleva a cabo la actualización:

`flask db upgrade`

### Creando nuevo modelo de comentarios

El objetivo: Permitir a los usuarios invitados añadir comentarios a un post.

- En `app/public` se crea `forms.py`
- En `app/models.py` se creará el nuevo modelo `Comment`
- Añadir una relación al modelo `Post` llamada `comments` para que desde el Post podamos acceder al listado de comentarios.
- En `app/public` actualizar `show_post()` para crear la instancia del formulario
- Modificar `public/post_view.html` para añadir el nuevo formulario de `CommentForm`
    - Adicionalmente agregue el filtro `safe` de jinja para mostrar el contenido html guardado en `content`
    ```html
        <h1>{{ post.title }}</h1>
        {{ post.content|safe }}
    ```
- Actualizar la BD
    - `flask db migrate -m "Nuevo modelo Comment"`
    - `flask db upgrade`


## Capítulo 12: Tests

¿Por qué son importantes los tests?
- Podemos introducir cambios en nuestro código asegurándonos, en cierta manera, de que todo sigue funcionado correctamente.
- Si algún test falla, podemos detectar los errores de forma prematura sin que estos lleguen al usuario.

### Conceptos básicos en los tests

Se hará uso del módulo incorporado de Python [unittest](https://docs.python.org/3/library/unittest.html)

Guía:
1. Una suite de tests se implementa dentro de una clase que hereda de `unittest.TestCase`.
2. Dentro de ella, cada método que definamos que comience por la palabra `test`, será considerado como un test independiente.
3. Puedes usar los métodos `setUp()` y `tearDown()` para ejecutar código antes y después de cada test.

Terminan luciendo como:

```python
import unittest

class TestSuite(unittest.TestCase):

    def setUp(self):
        # Código que se ejecuta antes de cada test
        ...

    def test_mi_test(self):
        # Código que se quiere probar
        ...

    def tearDown(self):
        # Código que se ejecuta después de cada test
        ...
```

4. Flask nos permite simular las peticiones de un cliente (ej: un navegador), ofreciéndonos un cliente, `app.test_client()` y manejando los contextos por nosotros.
    - En caso de que necesitemos el contexto fuera de una llamada del cliente, tendremos que crear el contexto:
    ```python
    with app.app_context():
        # Código que depende del contexto de aplicación
        # como el acceso a base de datos
    ```
5. Comprobar las variables para el entorno de testing en `config/testing.py`
6. Crear un nuevo directorio `tests` dentro de `app/`. Agregarle su fichero `__init__.py`


### Primer test en Flask

- Cremos dentro de `tests` el archivo `test_blog_client.py`
- Crear una clase de unittest Base llamada `BaseTestClass` para configurar el `setUp`y el `tearDown`
- Crear la primera prueba `BlogClientTestCase` en `test_blog_client.py`.
- Correr el test con `python -m unittest` (debe fallar)
- Abre el fichero `app/public/templates/public/index.html` (agregar `<li>No hay entradas</li>`)
- Volver a correr el test


### Test unitario

- Se probará el método `save` de `Post`. Para ello se necesita un usuario creado en la BD.
    - Porque en él se genera dinámicamente el campo `title_slug` de las entradas del blog.
    - En caso de guardar dos entradas con el mismo título, lo que ocurre es que el slug de la segunda añade un sufijo numérico incremental.
- Se crean dos nuevos usuarios en el contexto dentro de `setUp`.
- Añade un nuevo fichero llamado `test_post_model.py`.
- Debería fallar, ya que hace falta un [rollback](https://docs.sqlalchemy.org/en/20/orm/session_api.html#sqlalchemy.orm.Session.rollback) de la sesión cuando se produce el `InvalidRequestError`.
    - ¿Por qué `rollback`? Porque [la sesión se invalida](https://stackoverflow.com/a/25877950/2142093), entonces para crear una nueva transacción, se necesita hacer el `rollback`
    - Buen recurso: https://4geeks.com/lesson/everything-you-need-to-start-using-sqlalchemy



## Capítulo 13: Paginar las consultas de base de datos

Voy a crear una cantidad significativa de artículos de prueba para luego probar la paginación del tutorial.

### Generando datos random para el blog
- Hice uso del paquete de Python Faker para generar datos aleatorios de prueba. De esta forma me ahorraré escribir artículos.
    - Pypi: https://pypi.org/project/Faker/
    - Docs: https://faker.readthedocs.io/en/master/
- *Estaría chévere hacerlo con un LLM a ver cómo quedan.*
- Hice un comando por CLI de Flask para generar N cantidad de artículos
    - Docs: https://flask.palletsprojects.com/en/2.3.x/cli/

### Añadir una consulta paginada de base de datos al modelo Post

- Se necesita paginar ciertas consultas, ya que por ejemplo, en el caso tengamos 500 o 600 posts creados en BD. Si tuvieramos 100 ó 1000 usuarios haciendo esa consulta en el "/home", pondríamos en problemas el rendimiento de nuestra aplicación.

- Una consulta paginada tiene la misma forma que una consulta que devuelve todos los elementos de una tabla, con la diferencia de que define unos límites para obtener únicamente un subgrupo de elementos
    - La clase `BaseQuery` de `Flask-SQLAlchemy` nos ofrece el método `paginate()` que nos facilita la ejecución de este tipo de consultas.
    - Objeto Pagination --> https://flask-sqlalchemy.palletsprojects.com/en/3.1.x/api/#module-flask_sqlalchemy.pagination
    - Se ñade el método estático `all_paginated()` al final de la clase Post.

### Actualizar la vista index para hacer uso de la consulta paginada
- Modificar la vista `index()` en `app/public/routes.py`
```python

# Antes
posts = Post.get_all()
return render_template("public/index.html", posts=posts)

# Después
posts = Post.all_paginated(2, 3)
return render_template("public/index.html", posts=posts.items)
```

### Añadir controles de navegación a un listado paginado

- Se realizan los siguientes cambios a la vista `index`:
    - Se obtiene 'page' de la petición http. Lleva 1 por defecto.
    - Se pasa todo el objeto `Pagination`, ya no solo .items
```python
page = int(request.args.get('page', 1))
post_pagination = Post.all_paginated(page, 3)
return render_template("public/index.html", post_pagination=post_pagination)
```

- Ahora toca modificar la plantilla `app/public/templates/public/index.html`
    - En el ciclo for se cambia la variable `posts` por `post_pagination.items`
    - Para los controles de navegación se itera por `page in post_pagination.iter_pages()`

### Definir parámetros de configuración para la paginación
- Se mueve el segundo parámetro de `all_paginated()` como constance de la configuración.
    - Se crea la variable `ITEMS_PER_PAGE` en `config/default.py`
    - En la ruta, el código quedaría de la siguiente forma
    ```python
    per_page = current_app.config['ITEMS_PER_PAGE']
    post_pagination = Post.all_paginated(page, per_page)
    ```


## Capítulo 14: Enviar emails


### Configurar Flask-Mail para enviar emails en Flask


Se va a hacer uso del paquete Flask-Mail ---> https://pypi.org/project/Flask-Mail/

**Pasos de configuración:**


- Instalar el paquete en tu entorno virtual usando `pip install flask-mail`
- Crear objeto tipo `Mail` para configurar nuestro servidor de correo smtp en `app/__init__.py`
    - [¿Qué es SMTP?](https://aws.amazon.com/es/what-is/smtp/) 
    - [¿Qué son los Protocolos de Correo (POP3, SMTP e IMAP) y sus Puertos predeterminados?](https://www.siteground.es/tutoriales/email/protocolos-pop3-smtp-imap/)
```python
### archivo app/__init__.py

from flask_mail import Mail  # 1. Importamos la clase Mail

# Resto de imports
...

...
migrate = Migrate()
mail = Mail()  # 2. Instanciamos un objeto de tipo Mail


def create_app(settings_module):
    app = Flask(__name__, instance_relative_config=True)
    
    # Inicialización de la app
    ...

    ...
    db.init_app(app)
    migrate.init_app(app, db)
    mail.init_app(app)  # 3. Inicializamos el objeto mail

    ...
```

- Para que el objeto `Mail` pueda enviar mensajes, necesita conectarse a un *servidor smtp* de nuestra propiedad. Para ello, definiremos los siguientes parámetros de configuración en el fichero `config/default.py`:
    - Son los mismos parámetros que ya se le estaban pasando al objeto `SMTPHandler` en `/app/__init__.py`
```python

# Configuración del email
MAIL_SERVER = 'tu servidor smtp'  # Nombre de tu host smtp (Por ej: mail.smtp.com)
MAIL_PORT = 587  # Puerto de tu host smtp
MAIL_USERNAME = 'tu correo'  # Nombre de usuario del servidor smtp
MAIL_PASSWORD = 'tu contraseña'  # Contraseña del usuario del servidor smtp
DONT_REPLY_FROM_EMAIL = '(Juanjo, juanjo@j2logo.com)'  # emisor del correo
ADMINS = ('juanjo@j2logo.com', )
MAIL_USE_TLS = True  # Flag que indica si enviar los emails usando el protocolo TLS
```

- Con Flask-Mail solo se puede configurar una cuenta smtp. Sin embargo, utilizando una misma cuenta, sí que se pueden enviar emails desde diferentes direcciones de correo.

### Cómo crear un mensaje

- Crear un objeto `Message`
- Llamar al método `send()` del objeto `mail`
```python
from flask_mail import Message

from app import mail


msg = Message("Hola",
    sender=("Juanjo", "juanjo@j2logo.com"),  # emisor del correo
    recipients=["to@example.com"]  # lista de quien o quienes reciben el correo
)
```

- El contenido del mensaje lo indicamos en el atributo `body`, aunque también podemos crear un mensaje formateado empleando el lenguaje HTML.
- El correo se envía con `send()`:

```python
msg.body = 'Bienvenid@ a j2logo'
msg.html = '<p>Bienvenid@ a <strong>j2logo</strong></p>'
mail.send(msg)
```

### Enviar emails de forma asíncrona

- El problema de enviarlo como se explico antes, es que el envío se realiza de forma síncrona, en el mismo hilo en el que se procesa la petición web.
- Lo mejor es ejecutarlo en un hilo diferente, de manera que el envío del email no bloquee el flujo de la petición.

**Pasos:**

- Crea un nuevo paquete de python llamado `common` dentro de la carpeta `app`.
- En `common` crea un fichero llamado `mail.py`.
- Se crean dos funciones y mediante el módulo de Python `threading` se hace la ejecución en un hilo a parte.


### Probando el envío de emails desde el miniblog

- A diferencia del tutorial, creo que no implementaré el uso de envío de correos. Sin embargo, me gustaría tener una funcionalidad como en Django para imprimir por pantalla el mensaje si se está en un entorno local.
```python
# Realiza este ejercicio desde el shell de Python

# importa la app de flask y agregala al contexto
from entrypoint import app
app.app_context().push()

# imports necesarios para que funcione send_mail()
from flask import current_app
from app.common.mail import send_email

# variables dummy
name = 'Carmen'
email = 'carmen@example.com.co'


send_email(
    subject='Bienvenid@ al miniblog',
    sender=current_app.config['DONT_REPLY_FROM_EMAIL'],
    recipients=[email, ],
    text_body=f'Hola {name}, bienvenid@ al miniblog de Flask',
    html_body=f'<p>Hola <strong>{name}</strong>, bienvenid@ al miniblog de Flask</p>'
)

# si no tienes configurado una cuenta de correo real, verás un error similar al siguiente

'''
  File "/usr/local/Cellar/python@3.11/3.11.4_1/Frameworks/Python.framework/Versions/3.11/lib/python3.11/socket.py", line 962, in getaddrinfo
    for res in _socket.getaddrinfo(host, port, family, type, proto, flags):
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
socket.gaierror: [Errno 8] nodename nor servname provided, or not known
'''
```

- Te recomiendo usar servicios como [sendgrid.com](https://sendgrid.com/) que tienen planes gratuitos para probar sus funcionalidades.

## Capítulo 15: Trabajar con Fechas

Hay 2 modelos que usan fechas:
- Post
- Comment

Lo primero que se hará es mostrar la fecha de creación de cada `Post` en el index. Pasos:
1. Abrir el archivo `app/public/templates/public/index.html`
2. Agregar el siguiente HTML después del título
```HTML
{{ post.title }} <span class="postCreated">({{ post.created }})</span>
```
3. Les cambiaremos el formato a las fechas por uno más amigable y que no incluya el tiempo:
`{{ post.created.strftime('%d-%m-%Y') }}`


Para mayor información sobre strftime, te recomiendo esta página [https://strftime.org/](https://strftime.org/).


### Filtro de Flask para formatear fecha

*Una alternativa mejor a usar la función strftime en las plantillas para formatear una fecha, es hacer uso de los [filtros](https://jinja.palletsprojects.com/en/3.0.x/templates/#filters) que provee JINJA2.*

*Un filtro no es más que una función que modifica el valor de una variable y que se usa dentro de una plantilla.*

Creemos uno personalizado para las fechas:
1. Añade un nuevo fichero llamado `filters.py` al directorio `app/common/`
2. Agrega el código
```python
def format_datetime(value, format='short'):
    value_str = None
    if not value:
        value_str = ''
    if format == 'short':
        value_str = value.strftime('%d/%m/%Y')
    elif format == 'full':
        value_str = value.strftime('%B %d de %Y')
    else:
        value_str = ''
    return value_str
```

3. Abre el fichero `app/__init__.py` y, justo después de la función `create_app()`, añade la siguiente:
```python
from app.common.filters import format_datetime

def register_filters(app):
    '''registra la función format_datetime() como filtro de JINJA2.'''

    app.jinja_env.filters['datetime'] = format_datetime
```

4. Añade la siguiente línea dentro de la función `create_app()`, tras inicializar las extensiones y antes de registrar los Blueprins:
```python
# Registro de los filtros
register_filters(app)
```

5. Vayamos a `app/public/templates/public/index.html` y sustituye el strftime por:
`{{ post.created|datetime }}`

6. Ahora abre la plantilla `app/public/templates/public/post_view.html` y añade el sgte HTML justo después del tìtulo. De la siguiente forma:
```HTML
<h1>{{ post.title }}</h1>
<div>
    <span class="blogDate">{{ post.created|datetime('full') }}</span>
</div>
{{ post.content|safe }}
```
7. Para los comentarios, dentro de `post_view.html`, añade lo siguiente en la línea en que se muestra el nombre de la persona:
```HTML
<div>
    {{ comment.user_name }} comentó (<span class="blogDate">{{ comment.created|datetime }}</span>):
</div>
```

*Siempre que sea posible, definas un filtro para dar formato a las fechas. De esta forma puedes reutilizarlo en todas tus páginas y cualquier cambio que quieras/debas hacer estará localizado en un único punto.*

## Capítulo 16: Procesar ficheros

### Actualizar el modelo Post para guardar una imagen de cabecera

- En este paso necesitamos poder guardar imágenes y asociarlas cada una al post que le corresponde.
- En la base de datos guardaremos solamente el nombre que referencia al fichero en el sistema de archivos.
- Agreguemos un campo a `Post` para poder guardar el nombre de la imágen
```python
class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ...
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    image_name = db.Column(db.String, nullable=True)
    comments = ...
```

A diferencia del tutorial, agregué `, nullable=True` para que me deje crear artículos sin imágenes
- Luego de creado el campo, actualiza tu BD
    - `flask db migrate -m "añade imagen al modelo post"`
    - Por si las moscas puedes comprar la creacion del archivo de la migración dentro de `/migrations/versions/`
    - Llevamos a cabo la migración con: `flask db upgrade`

### Procesar ficheros en Flask de forma nativa

*Cuando se envía un formulario que contiene un fichero a una aplicación Flask, puedes acceder al fichero a través del diccionario `files` del objeto `request`*

Los elementos son de tipo [werkzeug.datastructures.FileStorage](https://werkzeug.palletsprojects.com/en/3.0.x/datastructures/#werkzeug.datastructures.FileStorage)

- Para verlo funcionando, se debe modificar el formulario que guarda el post (`app/admin/templates/admin/post_form.html`):
    - Indicar en el atributo `enctype` del HTML el valor `multipart/form-data`
    - Añade el campo `file`
    ```HTML
    <div>
        <label for="postImage">Imagen de cabecera:</label>
        <input type="file" id="postImage" name="post_image" accept="image/png, image/jpeg">
    </div>
    ```
    - Nótese que el nombre del campo es `post_image`
    - Ahora en `config/default.py` definamos un parámetro de config. para indicar donde se guardarán las imágenes (justo después de BASE_DIR)
    ```python
    from os.path import abspath, dirname, join

    # Media dir
    MEDIA_DIR = join(BASE_DIR, 'media')
    POSTS_IMAGES_DIR = join(MEDIA_DIR, 'posts')
    ```
    - Procesar el fichero en la vista `post_form()` dentro de `app/admin/routes.py`


Prueba subiendo la misma imagen del tutorial https://j2logo.com/wp-content/uploads/leccion16-procesar-ficheros-en-flask.jpg


### Procesar ficheros en Flask con Flask-WTF

Ahora veamos el mismo proceso pero con la extensión Flask-WTF (que usamos en la lección 3).

Los pasos a seguir serán modificar el FlaskForm, la plantilla HTML y luego la vista que publica el formulario.


1. Actualizar FlaskForm

```python
# Resto de imports
from flask_wtf.file import FileField, FileAllowed


class PostForm(FlaskForm):
    # ... resto de campos
    # nuevo campo
    post_image = FileField(
        'Imagen de cabecera', validators=[
        FileAllowed(['jpg', 'jpeg', 'png'], 'Solo se permiten imágenes')
    ])
    # ...
```

Asegúrate de añadir `jpeg` como formato válido (el tutorial no lo tiene).

2. Actualizar el HTML `post_form.html`

Quitamos las etiquetas que agregamos previamente para la imagen de la cabecera (dentro del formulario) y las cambiamos por las siguientes:
```HTML
...

<div>
    {{ form.post_image.label }}
    {{ form.post_image }}<br>
    {% for error in form.post_image.errors %}
    <span style="color: red;">{{ error }}</span>
    {% endfor %}
</div>

...
```

3. Actualizar la vista `post_form()`
    - Agregamos
    - Quitamos
```python
# resto de codigo

form = PostForm()

if form.validate_on_submit():
    title = form.title.data
    content = form.content.data
    file = form.post_image.data
    if file:
        image_name = None
        image_name = secure_filename(file.filename)
        images_dir = current_app.config['POSTS_IMAGES_DIR']
        # el resto de codigo sigue igual
```
El sutíl cambio fue obtener ahora el archivo desde la instancia `form` como se hizo con `content` y `title`. Además, ahora se valida solamente si `file` existe.

### Mostrar las imágenes en el blog

Añade en `entrypoint.py`

```python
from flask import send_from_directory

@app.route('/media/posts/<filename>')
def media_posts(filename):
    dir_path = os.path.join(
        app.config['MEDIA_DIR'],
        app.config['POSTS_IMAGES_DIR'])
    return send_from_directory(dir_path, filename)
```

Y ahora en `app/public/templates/public/post_view.html`

```HTML
{% if post.image_name %}
    <div>
        <img src="{{ url_for('media_posts', filename=post.image_name) }}" />
    </div>
{% endif %}

{{ post.content|safe }}
```

Realiza otra prueba para comprobar que todo este en orden (Ej: puedes crear un nuevo post con la imagen de la leccion 13 https://j2logo.com/wp-content/uploads/leccion3-formularios.jpg)

*Tomado directamente del tutorial*

```
Sin embargo, existen puntos de mejora en este procesamiento básico de formularios. Te detallo unos cuántos a continuación:

- Limitar el tamaño de la petición, tanto en el servidor web como en la propia aplicación Flask.
- Enviar un fichero mostrando barras de progreso.
- Servir los recursos públicos desde un servidor web y no desde la aplicación Flask.
- Implementar la lógica necesaria para guardar dos ficheros que se llamen igual. Actualmente, el método save() sobreescribe los archivos.
```

## Capítulo 17: Desplegar app en un entorno de producción

Se debe desplegar en producción ya que `Werkzeug` es una librería para ayudarnos localmente y como tal no es un servidor.

Es muy lenta para servir, ya que *una aplicación web en producción, tiene que ser capaz de soportar múltiples usuarios y solicitudes concurrentes.*

En esta sección hablaremos sobre:
- Servidores basados en [WSGI](https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface) como uWSGI, CherryPy o **Gunicorn**.
- Servidores web como Apache o **Nginx**.
    - Están pensados para un alto rendimiento
    - Manejar la seguridad
    - Poner en cola solicitudes/respuestas
    - Gestionar errores
    - Servir contenido estático y de otro tipo al mismo tiempo de manera muy eficiente.

### Nginx + Gunicorn

En esta guía usaremos Nginx como servidor web y Gunicorn como servidor de aplicaciones.

#### Nginx

El servidor web es el responsable de recibir todas las peticiones del cliente, en él se configuran los certificados para SSL/TLS, sirve contenido estático (css, js, imágenes, …) y manda las respuestas al cliente. Además, hace de proxy enviando todas las peticiones que tienen que ver realmente con la aplicación Flask al servidor de aplicaciones para que este las procese.

**Nginx** está optimizado para:
- Enrutar nombres de dominio (decide a dónde deben ir las solicitudes, o si una respuesta de error está en orden)
- Servir archivos estáticos
- Manejar muchas solicitudes que llegan a la vez
- Manejar clientes lentos
- Reenviar las solicitudes que deben ser dinámicas para Gunicorn
- Manejar las peticiones https

#### Gunicorn

El servidor de aplicaciones recibe las peticiones del cliente, a través del servidor web, y las transforma para que se ejecute el código correspondiente de la aplicación.

**Gunicorn** está pensado para

- Ejecutar un grupo de procesos/subprocesos de trabajo
- Traducir las solicitudes procedentes de Nginx (u otro servidor web) para que sean compatibles con WSGI
- Llamar al código de Python cuando llega una solicitud
- Traducir las respuestas WSGI de su aplicación en respuestas http adecuadas

### ¿Dónde desplegarlo?

Lo bueno es que existen múltiples opciones y para todos los gustos.
- AWS (la nube de Amazon)
- Digital Ocean
- Google Cloud
- Heroku
- Linode
- Python AnyWhere
- Render

### Preparar el servidor

### Desplegar Flask con Gunicorn

### Configurar Flask en Nginx + Gunicorn

### Conclusión
